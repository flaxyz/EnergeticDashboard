import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class RestService {

  constructor(private http: HttpClient) { }

  // var ipAddress = '172.16.32.72:8080'; 192.168.1.9

  getStatistics(){
     return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }

  getImage(ipAdd){
    // let imageServer = ipAddress;
      return this.http.get('http://' + ipAdd + '/image');
  }

}
